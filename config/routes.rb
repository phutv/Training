Rails.application.routes.draw do
  root 'posts#index'

  resources :posts do
    resource :like, module: :posts
  end

  resources :albums do
    resource :like, module: :albums
  end

  namespace :admin do
    resources :users
    get 'admin/user/change/:id', to: 'users#change_status', as: 'change_status'
  end

  get 'user/timeline/:id', to: 'albums#my_photos', as: 'timeline'

  devise_for :users
end
