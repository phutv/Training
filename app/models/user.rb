class User < ApplicationRecord
  USER_NAME_MAXLENGTH = 32
  USER_NAME_MINLENGTH = 4

  has_many :albums, dependent: :destroy
  has_many :posts, through: :albums, dependent: :destroy
  has_many :likes, as: :love, dependent: :destroy

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :user_name, presence: true,
                        length:
                        {
                          minimum: USER_NAME_MINLENGTH,
                          maximum: USER_NAME_MAXLENGTH
                        }

  after_create do |user|
    user.albums.create(title: 'Default', description: 'Default album')
  end

  def active_for_authentication?
    super && active?
  end

  def likes?(post)
    post.likes.where(user_id: id).any?
  end

  def likes_album?(album)
    album.likes.where(user_id: id).any?
  end
end
