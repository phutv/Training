class Post < ApplicationRecord
  STYLES_IMAGE = { medium: '300x300>', thumb: '100x100>' }.freeze
  CONTENT_TYPE_IMAGE = /\Aimage\/.*\Z/

  belongs_to :album
  has_many :likes, as: :love, dependent: :destroy

  validates :image, presence: true
  has_attached_file :image, styles: STYLES_IMAGE
  do_not_validate_attachment_file_type :image
  validates_attachment_content_type :image, content_type: CONTENT_TYPE_IMAGE

  scope :published, -> { where(album_id: Album.where(title: 'Default').pluck(:id), private: false) }
end
