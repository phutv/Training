class Album < ApplicationRecord
  belongs_to :user

  has_many :posts, dependent: :destroy
  has_many :likes, as: :love, dependent: :destroy

  scope :privated, -> { where.not(title: 'Default', private: true) }
  scope :published, ->(params_id) { where(user_id: params_id) }
end
