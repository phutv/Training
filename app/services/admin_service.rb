class AdminService
  def change_status_user(user)
    user.active = !user.active
    user.save
  end
end
