class LikeService
  def like(obj, id)
    obj.likes.where(user_id: id).first_or_create
  end

  def unlike(obj, id)
    obj.likes.where(user_id: id).delete_all
  end
end
