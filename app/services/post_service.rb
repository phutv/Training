class PostService
  def create_index(params)
    Post.published.page(params[:page])
  end

  def create_post(current_user, post_params)
    post = current_user.albums.first.posts.build(post_params)
    if post.save!
      return true
    else
      flash.now[:alert] = t(:try_again)
      return false
    end
  end
end
