class AlbumService
  def create_index(params)
    Album.privated.page(params[:page])
  end

  def create_dashbroad(params)
    Album.published(params[:id]).page params[:page]
  end

  def create_album_success(album, params)
    if album.private
      if params[:images]
        album.transaction do
          params[:images].each { |image| album.posts.create(image: image, private: true) }
        end
        end
    else
      if params[:images]
        album.transaction do
          params[:images].each { |image| album.posts.create(image: image) }
        end
        end
    end
  end

  def create_album_fail(_album)
    flash.now[:alert] = t(:try_again)
  end

  def create_album(current_user, album_params, params)
    album = current_user.albums.build(album_params)
    if album.save
      create_album_success(album, params)
      return true
    else
      create_album_fail(album)
      return false
    end
  end
end
