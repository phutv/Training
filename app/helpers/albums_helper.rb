module AlbumsHelper
  def owner_album(album)
    User.find_by_id(album.user_id)
  end
end
