module PostsHelper
  def owner_post(post)
    User.find_by_id(post.album.user_id)
  end
end
