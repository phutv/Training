class AlbumsController < ApplicationController
  before_action :authenticate_user!
  before_action :album_service
  before_action :set_album, only: %i[show edit update destroy]
  before_action :check_authority, only: %i[update destroy edit]

  def new
    @album = Album.new
  end

  def index
    @albums =  @album_service.create_index(params)
  end

  def my_photos
    @albums =  @album_service.create_dashbroad(params)
  end

  def create
    if @album_service.create_album(current_user, album_params, params)
      redirect_to albums_path
    else
      render :new
    end
  end

  def update
    @album.update(album_params)
    redirect_to albums_path
  end

  def destroy
    @album.destroy
    redirect_to albums_path
  end

  private

  def album_params
    params.require(:album).permit(:title, :description, :images, :private)
  end

  def set_album
    @album = Album.find(params[:id])
  end

  def check_authority
    (@album.user_id == current_user.id) || current_user.admin?
  end

  def album_service
    @album_service ||= AlbumService.new
  end
end
