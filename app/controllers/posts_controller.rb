class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :post_service
  before_action :set_post, only: %i[show edit update destroy]
  before_action :check_authority, only: %i[update destroy edit]

  def new
    @post = Post.new
  end

  def index
    @posts = @post_service.create_index(params)
  end

  def create
    if @post_service.create_post(current_user, post_params)
      redirect_to
    else
      render :new
    end
  end

  def update
    @post.update(post_params)
    redirect_to @post
  end

  def destroy
    @post.destroy
    redirect_to posts_path
  end

  private

  def post_params
    params.require(:post).permit(:title, :description, :image, :private)
  end

  def set_post
    @post = Post.find(params[:id])
  end

  def check_authority
    (@post.album.user_id == current_user.id) || current_user.admin?
  end

  def post_service
    @post_service ||= PostService.new
  end
end
