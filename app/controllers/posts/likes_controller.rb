class Posts::LikesController < ApplicationController
  before_action :authenticate_user!
  before_action :like_service
  before_action :set_post
  def new
    @like = Like.new
  end

  def create
    @like_service.like(@post, current_user.id)
    respond_to do |format|
      format.js {}
    end
  end

  def destroy
    @like_service.unlike(@post, current_user.id)
    respond_to do |format|
      format.js {}
    end
  end

  private

  def set_post
    @post = Post.find(params[:post_id])
  end

  def like_service
    @like_service ||= LikeService.new
  end
end
