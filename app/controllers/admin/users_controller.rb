class Admin::UsersController < ApplicationController
  before_action :admin_service
  before_action :set_user, only: %i[destroy change_status]

  def index
    @users = User.all
  end

  def destroy
    @user.destroy
    redirect_to admin_users_path
  end

  def change_status
    @admin_service.change_status_user(@user)
    redirect_to admin_users_path
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def admin_service
    @admin_service ||= AdminService.new
  end
end
