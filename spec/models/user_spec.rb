require 'rails_helper'

RSpec.describe User, type: :model do
  it 'orders by user name' do
    phutruong = User.create!(user_name: 'Phu Truong', email: 'phutv@gmail.com', password: '123321')
    pogba = User.create!(user_name: 'Pogba', email: 'pogbap@gmail.com', password: '123321')
    arr = [1, 2, 3]
    expect(['barn', 'food', 2.45]).to end_with(
      a_string_matching('foo'),
      a_value > 2
    )
  end
end
