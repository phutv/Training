require 'rails_helper'

RSpec.describe 'home page', type: :request do
  it "displays the user's username after successful login" do
    user = FactoryGirl.create(:user, user_name: 'Phu Truong', email: 'phutv@gmail.com', password: 'secret')
    visit '/login'
    fill_in 'Email', with: 'phutv@gmail.com'
    fill_in 'Password', with: 'secret'
    click_button 'Log in'

    expect(page).to have_selector('.header .email', text: 'phutv@gmail.com')
  end
end
