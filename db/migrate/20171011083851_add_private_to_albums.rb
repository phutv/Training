class AddPrivateToAlbums < ActiveRecord::Migration[5.1]
  def change
    add_column :albums, :private, :boolean, default: false
 end
end
