class EditLikes < ActiveRecord::Migration[5.1]
  def change
    add_reference :likes, :love, polymorphic: true, index: true
  end
end
